package com.example.ehsan.projectfactorycodingchallenge.ui.screenone

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.ehsan.projectfactorycodingchallenge.core.data.result.Event
import com.example.ehsan.projectfactorycodingchallenge.core.data.result.Result
import com.example.ehsan.projectfactorycodingchallenge.data.models.Thing
import com.example.ehsan.projectfactorycodingchallenge.data.thingrepository.ThingRepository
import javax.inject.Inject

class ScreenOneViewModel @Inject constructor(
    private val thingRepository: ThingRepository
) : ViewModel() {

    private val _thingList = MediatorLiveData<List<SelectableThing>>()
    val thingList: LiveData<List<SelectableThing>>
        get() = _thingList

    private val _navigateToNextScreen = MutableLiveData<Event<Unit>>()
    val navigateToNextScreen: LiveData<Event<Unit>>
        get() = _navigateToNextScreen

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    private val _error = MutableLiveData<Event<Int>>()
    val error: LiveData<Event<Int>>
        get() = _error

    init {
        loadThings()
    }

    private fun loadThings() {
        _thingList.addSource(thingRepository.getThings()) { result ->
            when (result) {
                is Result.Success -> {
                    _thingList.value = result.data.map { thing ->
                        SelectableThing(thing, false)
                    }
                    _loading.value = false
                }
                is Result.Error -> {
                    _error.value = Event(result.messageId())
                    _loading.value = false
                }
                is Result.Loading -> {
                    _loading.value = true
                }
            }
        }
    }

    fun onThingClicked(id: Int) {
        _thingList.value = _thingList.value?.apply {
            firstOrNull {
                it.thing.id == id
            }?.let {
                it.selected = it.selected.not()
            }
        }
    }

    fun nextScreenAllowed(list: List<SelectableThing>) = list.count { it.selected } >= 3

    fun selectedList() = _thingList.value?.filter {
        it.selected
    }?.map {
        it.thing
    } ?: error("The list must not be empty")
}

data class SelectableThing(
    val thing: Thing,
    var selected: Boolean
)