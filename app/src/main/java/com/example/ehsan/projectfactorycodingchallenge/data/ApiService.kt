package com.example.ehsan.projectfactorycodingchallenge.data

import com.example.ehsan.projectfactorycodingchallenge.data.models.api.UsersItem
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET("/users")
    fun getEmployees(): Call<ArrayList<UsersItem>>
}