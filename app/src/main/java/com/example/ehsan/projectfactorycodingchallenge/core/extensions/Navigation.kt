package com.example.ehsan.projectfactorycodingchallenge.core.extensions

import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import timber.log.Timber

fun Fragment.navigate(navDirections: NavDirections) {
    try {
        findNavController().navigate(navDirections)
    } catch (e: Exception) {
        Timber.e(e)
    }
}

fun Fragment.navigateUp() {
    findNavController().navigateUp()
}