package com.example.ehsan.projectfactorycodingchallenge.data.thingrepository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.ehsan.projectfactorycodingchallenge.R
import com.example.ehsan.projectfactorycodingchallenge.core.data.result.Result
import com.example.ehsan.projectfactorycodingchallenge.data.ApiService
import com.example.ehsan.projectfactorycodingchallenge.data.models.Thing
import com.example.ehsan.projectfactorycodingchallenge.data.models.api.UsersItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

interface ThingRepository {
    fun getThings(): LiveData<Result<List<Thing>>>
}

@Singleton
class DefaultThingRepository @Inject constructor(private val apiService: ApiService) : ThingRepository {
    override fun getThings(): LiveData<Result<List<Thing>>> {
        val result = MutableLiveData<Result<List<Thing>>>()
        result.value = Result.Loading

        apiService.getEmployees().enqueue(object : Callback<ArrayList<UsersItem>> {
            override fun onResponse(
                call: Call<ArrayList<UsersItem>>,
                response: Response<ArrayList<UsersItem>>
            ) {
                response.body()?.let {
                    result.value = Result.Success(it.mapNotNull { it.toThing() }.take(7))
                } ?: run {
                    Timber.e("Response body is null")
                    result.value = Result.Error(throwable = null, messageId = R.string.unknown_error)
                }
            }

            override fun onFailure(call: Call<ArrayList<UsersItem>>, t: Throwable) {
                Timber.e(t)
                result.value = Result.Error(t, R.string.network_error)
            }

        })

        return result
    }
}