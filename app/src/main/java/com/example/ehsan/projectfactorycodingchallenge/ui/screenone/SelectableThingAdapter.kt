package com.example.ehsan.projectfactorycodingchallenge.ui.screenone

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ehsan.projectfactorycodingchallenge.R
import com.example.ehsan.projectfactorycodingchallenge.core.extensions.inflateRecyclerAdapterLayout
import com.example.ehsan.projectfactorycodingchallenge.core.extensions.invisible
import com.example.ehsan.projectfactorycodingchallenge.core.extensions.visible
import kotlinx.android.synthetic.main.item_selectable_thing.view.*

class SelectableThingAdapter : RecyclerView.Adapter<SelectableThingAdapter.ThingItemViewHolder>() {

    var onItemClicked: (Int) -> Unit = {}

    var data: List<SelectableThing> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThingItemViewHolder =
        ThingItemViewHolder(parent.inflateRecyclerAdapterLayout(R.layout.item_selectable_thing))

    override fun onBindViewHolder(holder: ThingItemViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    inner class ThingItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(selectableThing: SelectableThing) {
            with(itemView) {
                thingTextView.text = selectableThing.thing.name
                if (selectableThing.selected) {
                    checkImageView.visible()
                } else {
                    checkImageView.invisible()
                }

                setOnClickListener {
                    onItemClicked(selectableThing.thing.id)
                }
            }
        }
    }
}
