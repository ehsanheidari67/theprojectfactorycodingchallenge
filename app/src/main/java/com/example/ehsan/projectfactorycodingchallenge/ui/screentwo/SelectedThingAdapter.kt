package com.example.ehsan.projectfactorycodingchallenge.ui.screentwo

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ehsan.projectfactorycodingchallenge.R
import com.example.ehsan.projectfactorycodingchallenge.core.extensions.inflateRecyclerAdapterLayout
import com.example.ehsan.projectfactorycodingchallenge.core.extensions.invisible
import com.example.ehsan.projectfactorycodingchallenge.data.models.Thing
import kotlinx.android.synthetic.main.item_selectable_thing.view.*

class SelectedThingAdapter : RecyclerView.Adapter<SelectedThingAdapter.ThingItemViewHolder>() {

    var data: List<Thing> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThingItemViewHolder =
        ThingItemViewHolder(parent.inflateRecyclerAdapterLayout(R.layout.item_selectable_thing))

    override fun onBindViewHolder(holder: ThingItemViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    class ThingItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(thing: Thing) {
            with(itemView) {
                thingTextView.text = thing.name
                checkImageView.invisible()
            }
        }
    }
}
