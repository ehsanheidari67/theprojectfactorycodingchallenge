package com.example.ehsan.projectfactorycodingchallenge.core.di

import com.example.ehsan.projectfactorycodingchallenge.core.di.viewmodel.BindViewModels
import com.example.ehsan.projectfactorycodingchallenge.core.di.viewmodel.ViewModelModule
import com.example.ehsan.projectfactorycodingchallenge.ui.screenone.ScreenOneFragment
import com.example.ehsan.projectfactorycodingchallenge.ui.screentwo.ScreenTwoFragment
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        ViewModelModule::class,
        BindViewModels::class
    ]
)
interface ApplicationComponent {
    fun injectScreenOneFragment(fragment: ScreenOneFragment)
    fun injectScreenTwoFragment(fragment: ScreenTwoFragment)
}