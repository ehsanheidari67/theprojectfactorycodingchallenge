package com.example.ehsan.projectfactorycodingchallenge.core.di.viewmodel

import androidx.lifecycle.ViewModel
import com.example.ehsan.projectfactorycodingchallenge.ui.screenone.ScreenOneViewModel
import com.example.ehsan.projectfactorycodingchallenge.ui.screentwo.ScreenTwoViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class BindViewModels {
    @Binds
    @IntoMap
    @ViewModelKey(ScreenOneViewModel::class)
    abstract fun bindScreenOneViewModel(viewModel: ScreenOneViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScreenTwoViewModel::class)
    abstract fun bindScreenTwoViewModel(viewModel: ScreenTwoViewModel): ViewModel
}
