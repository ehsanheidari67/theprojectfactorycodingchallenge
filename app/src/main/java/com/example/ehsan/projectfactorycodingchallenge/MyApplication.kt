package com.example.ehsan.projectfactorycodingchallenge

import android.app.Application
import com.example.ehsan.projectfactorycodingchallenge.core.di.ApplicationComponent
import com.example.ehsan.projectfactorycodingchallenge.core.di.ApplicationModule
import com.example.ehsan.projectfactorycodingchallenge.core.di.DaggerApplicationComponent
import com.jakewharton.threetenabp.AndroidThreeTen
import timber.log.Timber

class MyApplication : Application() {
    val component: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this)).build()
    }

    override fun onCreate() {
        super.onCreate()
        setupThreeTen()
        setupTimber()
    }

    private fun setupThreeTen() {
        AndroidThreeTen.init(this)
    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}