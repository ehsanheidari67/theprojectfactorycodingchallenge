package com.example.ehsan.projectfactorycodingchallenge.ui.screentwo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ehsan.projectfactorycodingchallenge.MyApplication
import com.example.ehsan.projectfactorycodingchallenge.R
import com.example.ehsan.projectfactorycodingchallenge.core.extensions.navigateUp
import kotlinx.android.synthetic.main.fragment_screen_one.recyclerView
import kotlinx.android.synthetic.main.fragment_screen_two.*
import javax.inject.Inject

class ScreenTwoFragment : Fragment() {

    private val args by navArgs<ScreenTwoFragmentArgs>()

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<ScreenTwoViewModel> { viewModelFactory }

    private val adapter: SelectedThingAdapter by lazy {
        SelectedThingAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity?.application as MyApplication).component.injectScreenTwoFragment(this)
        args.thingList.toList().let {
            viewModel.randomlyChooseThingAndSet(it)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_screen_two, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUi()
        startObserving()
    }

    private fun setupUi() {
        setupRecyclerView()
        backButton.setOnClickListener {
            navigateUp()
        }
    }

    private fun startObserving() {
        viewModel.thingList.observe(viewLifecycleOwner) {
            adapter.data = it
        }

        viewModel.thing.observe(viewLifecycleOwner) {
            chosenThingTextView.text = it.name
        }
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        recyclerView.adapter = adapter
    }
}