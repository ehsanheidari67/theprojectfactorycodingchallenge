package com.example.ehsan.projectfactorycodingchallenge.ui.screentwo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.ehsan.projectfactorycodingchallenge.data.models.Thing
import javax.inject.Inject

class ScreenTwoViewModel @Inject constructor() : ViewModel() {

    private val _thingList = MutableLiveData<List<Thing>>()
    val thingList: LiveData<List<Thing>>
        get() = _thingList

    private val _thing = MutableLiveData<Thing>()
    val thing: LiveData<Thing>
        get() = _thing

    fun randomlyChooseThingAndSet(list: List<Thing>) {
        val chosenThing = chooseThingRandomly(list)

        _thingList.value = list.filterNot { chosenThing.id == it.id }
        _thing.value = chosenThing
    }

    private fun chooseThingRandomly(list: List<Thing>) = list.random()
}