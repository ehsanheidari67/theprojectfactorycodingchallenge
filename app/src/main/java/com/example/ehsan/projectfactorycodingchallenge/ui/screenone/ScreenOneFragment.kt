package com.example.ehsan.projectfactorycodingchallenge.ui.screenone

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ehsan.projectfactorycodingchallenge.MyApplication
import com.example.ehsan.projectfactorycodingchallenge.R
import com.example.ehsan.projectfactorycodingchallenge.core.data.result.EventObserver
import com.example.ehsan.projectfactorycodingchallenge.core.extensions.invisible
import com.example.ehsan.projectfactorycodingchallenge.core.extensions.navigate
import com.example.ehsan.projectfactorycodingchallenge.core.extensions.visible
import kotlinx.android.synthetic.main.fragment_screen_one.*
import javax.inject.Inject

class ScreenOneFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<ScreenOneViewModel> { viewModelFactory }

    private val adapter: SelectableThingAdapter by lazy {
        SelectableThingAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity?.application as MyApplication).component.injectScreenOneFragment(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_screen_one, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUi()
        startObserving()
    }

    private fun setupUi() {
        setupRecyclerView()
        nextButton.setOnClickListener {
            navigate(ScreenOneFragmentDirections.toScreenTwoFragment(viewModel.selectedList().toTypedArray()))
        }
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        adapter.onItemClicked = { id ->
            viewModel.onThingClicked(id)
        }
        recyclerView.adapter = adapter
    }

    private fun startObserving() {
        viewModel.thingList.observe(viewLifecycleOwner) { list ->
            adapter.data = list.also {
                showNextButton(viewModel.nextScreenAllowed(it))
            }
        }

        viewModel.error.observe(viewLifecycleOwner, EventObserver { messageId ->
            Toast.makeText(context, messageId, Toast.LENGTH_LONG).show()
        })

        viewModel.loading.observe(viewLifecycleOwner) { loading ->
            if (loading) {
                progressBar.visible()
            } else {
                progressBar.invisible()
            }
        }

        viewModel.navigateToNextScreen.observe(viewLifecycleOwner, EventObserver {

        })
    }

    private fun showNextButton(show: Boolean) {
        if (show) {
            nextButton.visible()
        } else {
            nextButton.invisible()
        }
    }
}