package com.example.ehsan.projectfactorycodingchallenge.core.extensions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

private fun ViewGroup?.inflateLayout(resource: Int, attachToRoot: Boolean): View = LayoutInflater.from(this?.context).inflate(resource, this, attachToRoot)

fun ViewGroup?.inflateRecyclerAdapterLayout(resource: Int): View = inflateLayout(resource, false)

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun View.visible() {
    visibility = View.VISIBLE
}
