package com.example.ehsan.projectfactorycodingchallenge.core.di

import android.app.Application
import android.content.Context
import com.example.ehsan.projectfactorycodingchallenge.data.ApiService
import com.example.ehsan.projectfactorycodingchallenge.data.thingrepository.DefaultThingRepository
import com.example.ehsan.projectfactorycodingchallenge.data.thingrepository.ThingRepository
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: Application) {
    @Singleton
    @Provides
    fun provideContext(): Context = application

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl("https://jsonplaceholder.typicode.com")
        .client(OkHttpClient.Builder().build())
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
        .build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService =
        retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun provideThingRepository(thingRepository: DefaultThingRepository): ThingRepository =
        thingRepository
}