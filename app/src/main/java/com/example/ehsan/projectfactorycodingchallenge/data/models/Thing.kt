package com.example.ehsan.projectfactorycodingchallenge.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Thing(val id: Int, val name: String) : Parcelable